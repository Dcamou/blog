import React, { useState } from 'react';
import Article from '../components/Articles';
import { articles } from '../datas/ArticlesDatas';
import '../styles/Blog.css'

const Blog = () => {
    const [articleList, setArticleList] = useState(articles);
    const [newArticle, setNewArticle] = useState({ title: '', text: '' });

    const addArticle = () => {
        if (newArticle.title && newArticle.text) {
            setArticleList([...articleList, newArticle]);
            setNewArticle({ title: '', text: '' });
        }
};

const deleteArticle = (id) => {
    const updatedArticles = articleList.filter((article) => article.id !== id);
    setArticleList(updatedArticles);
};

const editArticle = (id, newText) => {
    const updatedArticles = articleList.map((article) => {
        if (article.id === id) {
            return { ...article, text: newText };
        }
    return article;
    });
    setArticleList(updatedArticles);
};

return (
    <div className='blog'>
        <h1>Mon Blog Personnel</h1>
        <div>
            <input
                type="text"
                className="input-title" // Utilisez des classes CSS définies dans Blog.css
                placeholder="Title"
                value={newArticle.title}
                onChange={(e) => setNewArticle({ ...newArticle, title: e.target.value })}
            />
            <textarea
                placeholder="Text"
                value={newArticle.text}
                onChange={(e) => setNewArticle({ ...newArticle, text: e.target.value })}
            />
            <button onClick={addArticle} id='Imj-Add'>Ajouter un Article</button>
        </div>
        {articleList.map((article, index) => (
            <Article
                key={index}
                article={article}
                onDelete={deleteArticle}
                onEdit={editArticle}
            />
        ))}
    </div>
)};

export default Blog;
