import React, { useState } from 'react';
import '../styles/Articles.css';

const Article = ({ article, onDelete, onEdit }) => {
    const [isEditing, setEditing] = useState(false);
    const [editedText, setEditedText] = useState(article.text);

    const handleEdit = () => {
        onEdit(article.id, editedText);
        setEditing(false);
    };

    return (
        <div>
            <h2>{article.title}</h2>
            {isEditing ? (
            <div>
                <textarea
                    value={editedText}
                    onChange={(e) => setEditedText(e.target.value)}
                />
                <button onClick={handleEdit}>Sauvegarder</button>
            </div>
            ) : (
            <p>{article.text}</p>
            )}
            <button onClick={() => onDelete(article.id)}>Supprimer</button>
            <button onClick={() => setEditing(!isEditing)}>
                {isEditing ? 'Cancel' : 'Edit'}
            </button>
        </div>
    );
};

export default Article;
