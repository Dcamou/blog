export const articles = [
    {
        id: 1,
        title: "Les Coachs de Bakeli",
        text: "Ils sont vraiment formidable tous sans exception.",
    },
    {
        id: 2,
        title: "Les étudiants de Bakeli",
        text: "Nous avons l'obligation d'etre parmis les meilleurs en informatique vu la qualité du coaching et le savoir faire qu'on y acquiert.",
    },
    {
        id: 3,
        title: "Les cours à Bakeli School",
        text: "Le plateforme des cours est trés riche. On y trouve de superbes tutoriels qui nous aident à acquerir des compétences et connaissances en informatique et plus particulièrement en developpement.",
    }
];
